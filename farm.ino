// Ethernet shield libraries
#include <Ethernet.h>
#include <ArduinoJson.h>

#include <SPI.h>

// RTC Library
#include <DS3231_Simple.h>
#include <EEPROM.h>

// DHT Library
#include "DHT.h"
#define DHTPIN 2  
#define DHTTYPE DHT22

DHT dht(DHTPIN, DHTTYPE);

DS3231_Simple Clock;

// Variabili per connessione

byte mac[] = {0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED};
byte ip[] = {192, 168, 1, 101};
String readString;
EthernetServer server(80);

int checkSoil_1() {
   digitalWrite(4, HIGH);
   delay(100);
   int sensorValue = analogRead(A0); 
   digitalWrite(4, LOW);
   return sensorValue;
}

int checkSoil_2() {
   digitalWrite(7, HIGH);
   delay(100);
   int sensorValue = analogRead(A1); 
   digitalWrite(7, LOW);
   return sensorValue;
}

String getNecessaryDateTime() {
  DateTime MyDateAndTime;
  MyDateAndTime = Clock.read();
  String str1 = "";
  String str2 = str1 + MyDateAndTime.Hour + ":" + MyDateAndTime.Minute;
  return str2;
}

void irrigate(int myhour, int myminute) {
  
  Serial.print("Time: ");
  Serial.print(myhour);
  Serial.print(":");
  Serial.println(myminute);
  
  if((myhour == 22 && myminute == 4)){
    int mid = (checkSoil_1()+checkSoil_2())/2;
    while(mid >= 450) {
      digitalWrite(9, LOW);
      mid = (checkSoil_1()+checkSoil_2())/2;
      Serial.print("Soil mid: ");
      Serial.println(mid);
    }
    digitalWrite(9, HIGH);
    writeOnTheEEPROM(getNecessaryDateTime());
  } else {
    Serial.println("It's not time to irrigate");
  }
}

int splitHourAndMinute(String stringToSplit, int wherePutSplittedData[]) {
  
  int str_len = stringToSplit.length() + 1;
  char char_array[str_len];
  stringToSplit.toCharArray(char_array, str_len);
 
  String hours, minutes;
  int i = 0;
  while(char_array[i] != ':'){
    hours += char_array[i];
    i++;
  }
  wherePutSplittedData[0] = hours.toInt();
  int j = i + 1;
  while(char_array[j] != '\0'){
    minutes += char_array[j];
    j++;
  }
  wherePutSplittedData[1] = minutes.toInt();
  return wherePutSplittedData;
}

void clearTheEEPROM(){
   for (int i = 0 ; i < 8 ; i++) {
    EEPROM.write(i, 0);
  }
}

void writeOnTheEEPROM(String stringToWrite) {
  int str_len = stringToWrite.length() + 1;
  char char_array[str_len];
  stringToWrite.toCharArray(char_array, str_len);
  clearTheEEPROM();
  int i = 0; 
  while(char_array[i] != '\0'){
    EEPROM.write(i, char_array[i]);
    i++;
  }
  EEPROM.write(i + 1 , '\0');
}

String readFromTheEEPROM() {
  String string;
  char character;
  int i = 0;
  while(EEPROM.read(i) != '\0'){
    character = EEPROM.read(i);
    string += character;
    i++;
  }
  return string;
}

void makeEmpty(int afterHowManyMinutes, int hourNow, int minuteNow) {
  if(readFromTheEEPROM() != ""){
    
    int data[2];
    String str = readFromTheEEPROM();
    splitHourAndMinute(str, data);
    int hourThen = data[0];
    int minuteThen = data[1];

    if((minuteNow - minuteThen) >= afterHowManyMinutes || (hourNow - hourThen) > 0){
      digitalWrite(8, LOW);
      delay(90000);
      digitalWrite(8, HIGH);
      clearTheEEPROM();
    } else {
      Serial.print("Time left before empty: ");
      Serial.print(afterHowManyMinutes - (minuteNow - minuteThen));
      Serial.println(" m");
    }
  }
}
void EthernetSetup() {
  Ethernet.begin(mac, ip);
  if (Ethernet.hardwareStatus() == EthernetNoHardware) {
    Serial.println("Ethernet shield was not found. Running without connectivity");
  }
  if (Ethernet.linkStatus() == LinkOFF) {
    Serial.println("Ethernet cable is not connected.");
  }
  server.begin();
  Serial.print(F("Please send requests to http://"));
  Serial.println(Ethernet.localIP());
}

void setup() {
  Serial.begin(9600);
  
  dht.begin();
  Clock.begin();
  EthernetSetup();
  
  pinMode(4, OUTPUT); // Pin corrente per l'igrometro 1
  pinMode(7, OUTPUT); // Pin corrente per l'igrometro 2
  
  pinMode(9, OUTPUT); // Pin per la pompa
  digitalWrite(9, HIGH); // SPEGNI
  
  pinMode(8, OUTPUT); // Pin per la valvola
  digitalWrite(8, HIGH);
 }

void stato(EthernetClient client, float temperatura, float umidita) {
  StaticJsonDocument<200> doc;
  doc["Temperatura"] = temperatura;
  doc["Umidita"] = umidita;
  doc["Soil_1"] = checkSoil_1();
  doc["Soil_2"] = checkSoil_2();
  Serial.print(F("Sending: "));
  serializeJson(doc, Serial);
  Serial.println();
  // Write response headers
  client.println(F("HTTP/1.0 200 OK"));
  client.println(F("Content-Type: application/json"));
  client.println(F("Connection: close"));
  client.print(F("Content-Length: "));
  client.println(measureJsonPretty(doc));
  client.println();
  serializeJsonPretty(doc, client);
}

void paginaNonTrovata(EthernetClient client) {
  client.println("HTTP/1.1 404 NOT FOUND");
  client.println();
}

void webServer(float temperatura, float umidita) {
  EthernetClient client = server.available();
  if (client) {
    Serial.println("new client");
    boolean currentLineIsBlank = true;
    while (client.connected()) {
      if (client.available()) {
        char c = client.read();
        Serial.write(c);
        if (readString.length() < 100) {
          readString += c;
        }
        if (c == '\n' && currentLineIsBlank) {
          if (readString.indexOf("GET /Stato") == 0) stato(client, temperatura, umidita);
          else paginaNonTrovata (client);
          readString = "";
          break;
        }
        if (c == '\n') {
          currentLineIsBlank = true;
        } else if (c != '\r') {
          currentLineIsBlank = false;
        }
      }
    }
    delay(1);
    client.stop();
    Serial.println("client disconnected");
  }
}
void loop() {
  float temp = dht.readTemperature(), hum = dht.readHumidity();
  webServer(temp, hum);

  
  DateTime MyDateAndTime;
  MyDateAndTime = Clock.read();
  int myhour = MyDateAndTime.Hour;
  int myminute = MyDateAndTime.Minute;
  
  irrigate(myhour, myminute);
  makeEmpty(5, myhour, myminute);
}